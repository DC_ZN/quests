#!/usr/bin/env bash

################################################################################
## CONFIGURATION ###############################################################

BUILDDIR='/root/tel-yo-gram'
PARAMS_FILE="${BUILDDIR}/changeable.properties"
RELEASE="${BUILDDIR}/release/TelYoGram.apk"
# BUILD="docker run --tty --interactive --volume=$(pwd):/opt/workspace --workdir=/opt/workspace -v android-sdk:/android-sdk-linux -v gradle-cache:/root/.gradle -v android-cache:/root/.android --rm cangol/android-gradle  /bin/sh -c './gradlew assembleRelease'"

################################################################################

NORM=0
BOLD=1
UNLN=4
RED=31
GREEN=32
BROWN=33
BLUE=34
MAG=35
CYAN=36
GREY=37

################################################################################

show() {
    if [[ -n "$2" ]] ; then
        echo -e "\e[${2}m${1}\e[0m"
    else
        echo -e "$1"
    fi
}

usage() {
  show "sh $0 simething.onion /path/to/apk/uuidv4.apk" $BROWN
  show "Example: sh $0 example.onion /root/deploy/quests/generated/djurshga.apk"  $BROWN
}

checkcmdresult() {
  if [ $? -ne 0 ]; then
    show "ERROR: Something wrong ..." $RED
  fi
}

check_dir() {
  local fl=${1}
  touch ${fl}
  if [ $? -ne 0 ];then
    show "Can't write ${fl} file." $RED
    show "Please check permissions" $RED
    exit 1
  else
    rm ${fl}
  fi
}

replace_params() {
  local url="${1}"
  local params_file="${2}"

  sed -i "s/connection=.*/connection=${url}:4000/g" ${params_file}
}

make_build() {
  docker run --tty --interactive --volume=$(pwd):/opt/workspace --workdir=/opt/workspace -v android-sdk:/android-sdk-linux -v gradle-cache:/root/.gradle -v android-cache:/root/.android --rm cangol/android-gradle  /bin/sh -c "./gradlew assembleRelease"
}

copy_artifact() {
  local src="${1}"
  local dest="${2}"

  cp ${1} ${2}
}

main() {
    DATE=`date +%Y%m%d%H%m`
    if [ $# -ne 2 ]; then
      usage
      exit 1
    else
      ONION="${1}"
      APK_PATH="${2}"
    fi

    show "\n==== Preparation ================================================================\n" $BOLD
    show "Checking apk place ..." ${BOLD}
    check_dir ${APK_PATH}
    show ""

    show "==== Processing =================================================================\n" $BOLD

    show "Changing build params ..." ${BOLD}
    replace_params "${ONION}" "${PARAMS_FILE}"
    show "Building ..." ${BOLD}
    pushd ${BUILDDIR} > /dev/null
    make_build
    checkcmdresult
    popd > /dev/null
    show "Coping apk ..." ${BOLD}
    copy_artifact "${RELEASE}" "${APK_PATH}"

    show "\nComplete." $GREEN
}

###############################################################################

main "$@"
